#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


enum Suit 
{
	//Ordered so that standard convention of using alphabetical order can be applied 
	// Clubs < Diamonds < Hearts < Spades in case of rank tie - ace of spades beats ace of clubs)
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES

};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card 
{
	Suit Suit;
	Rank Rank;
};


//Prototypes
Card HighCard(Card card1, Card card2);
void PrintCard(Card card);


int main()
{
	Card c1, c2;
	c1.Rank = FIVE;
	c1.Suit = HEARTS;
	c2.Rank = QUEEN;
	c2.Suit = SPADES;

	cout << "Card1: ";
	PrintCard(c1);

	cout << "Card2: ";
	PrintCard(c2);


	_getch();
	return 0;
}

Card HighCard(Card card1, Card card2) {
	//return rank
	if (card1.Rank > card2.Rank) {
		return card1;
	}
	else if (card1.Rank < card2.Rank) {
		return card2;
	}
	else {
		if (card1.Suit > card2.Suit) {
			return card1;
		}
		else
		{
			return card2;
		}
	}
}

//Print card
void PrintCard(Card card) {
	string rankText = "", suitText = "";
	switch (card.Rank) {
	case TWO: rankText = "2"; break;
	case THREE: rankText = "3"; break;
	case FOUR: rankText = "4"; break;
	case FIVE: rankText = "5"; break;
	case SIX: rankText = "6"; break;
	case SEVEN: rankText = "7"; break;
	case EIGHT: rankText = "8"; break;
	case NINE: rankText = "9"; break;
	case TEN: rankText = "10"; break;
	case JACK: rankText = "Jack"; break;
	case QUEEN: rankText = "Queen"; break;
	case KING: rankText = "King"; break;
	case ACE: rankText = "Ace"; break;
	}
	switch (card.Suit) {
	case CLUBS: suitText = "Clubs"; break; 
	case DIAMONDS: suitText = "Diamonds"; break;
	case HEARTS: suitText = "Hearts"; break; 
	case SPADES: suitText = "Spades"; break;
	}
	std::cout << rankText << " of " << suitText << "\n";
}
